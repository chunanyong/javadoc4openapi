import com.thoughtworks.qdox.model.*;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.springrain.javadoc4openapi.JavaDoc4OpenAPI;

import java.util.List;


public class SwaggerTest {

    @Test
    public void test1(){
        JavaDoc4OpenAPI swaggerSourceParse=new JavaDoc4OpenAPI();
        List<JavaClass> controllerJavaClassList = swaggerSourceParse.controllerJavaClassList();


        for (JavaClass javaClass : controllerJavaClassList) {


            //获取类的注释
            System.out.println("====获取类的注释====");
            System.out.println(javaClass.getComment());
            System.out.println("====获取类上的泛型====");
            System.out.println(javaClass.getTypeParameters());
            List<DocletTag> classTags = javaClass.getTags();
            for(DocletTag docletTag:classTags){
                System.out.println(docletTag.getName() + "," + docletTag.getValue());
            }

            System.out.println("\n");
            System.out.println("====获取类的注解====");

            List<JavaAnnotation> javaAnnotation = javaClass.getAnnotations();

            for (JavaAnnotation annotation:javaAnnotation){
                System.out.println(annotation.getType().toString() + "," + annotation.getNamedParameterMap());
            }



            System.out.println("\n");

            System.out.println("====类的引用====");

            javaClass.getBeanProperties(true);



            //获取继承的父类
            System.out.println("====获取继承的父类====");
            System.out.println(javaClass.getSuperClass() + "\n");

            //获取接口
            System.out.println("====获取接口====");
            System.out.println(javaClass.getImplements() + "\n");

            //获取方法
            System.out.println("====获取方法====");
            List<JavaMethod> methods = javaClass.getMethods();
            System.out.println(methods + "\n");

            for (JavaMethod javaMethod :methods) {

                System.out.println("====获取方法参数类型====");
                System.out.println(javaMethod.getParameterTypes(true).get(0) + "\n");

                //获取注解
                System.out.println("====获取方法注解====");
                List<JavaAnnotation> methodAnnotation = javaMethod.getAnnotations();
                System.out.println(methodAnnotation + "\n");

                //get Login方法
               // JavaMethod javaMethod = methods.get(0);
                //方法返回类型
                System.out.println("====获取方法返回类型====");
                System.out.println(javaMethod.getReturns() + "\n");
                //获取参数
                System.out.println("====获取参数====");
                List<JavaParameter> parameters = javaMethod.getParameters();
               // System.out.println(parameters);
                //获取参数类型

                    for (JavaParameter parameter:parameters ){
                    //get userName参数
                   // JavaParameter parameter = parameters.get(0);

                    //参数名称
                        System.out.println(parameter.getName());
                        System.out.println("====获取参数泛型====");
                       JavaType base= parameter.getType();
                        if ( base instanceof JavaParameterizedType ) {
                            List<JavaType> argus=( (JavaParameterizedType) base ).getActualTypeArguments();

                            if (CollectionUtils.isNotEmpty(argus)){
                                JavaType javaType = argus.get(0);
                                System.out.println("====获取参数泛型的声明类型====");
                                System.out.print(((JavaClass)javaType).getTypeParameters()+"---");
                                System.out.print(javaType+"---");
                                List<JavaType> argus2= ((JavaParameterizedType)javaType).getActualTypeArguments();
                                if (CollectionUtils.isNotEmpty(argus2)) {
                                    System.out.println(argus2.get(0));
                                }
                            }


                        }
                       System.out.println(parameter.getGenericCanonicalName());
                        System.out.println(parameter.getResolvedGenericFullyQualifiedName());
                        System.out.println(parameter.getType().getGenericValue());
                        System.out.println(parameter.getGenericFullyQualifiedName());

                    //参数类型
                    System.out.println(parameter.getFullyQualifiedName() + "\n");
                }
                System.out.println("====获取方法注释====");
                //获取方法注释
                System.out.println(javaMethod.getComment());
                //获取参数备注
                List<DocletTag> tags = javaMethod.getTags();
                tags.forEach(item -> {
                    System.out.println(item.getName() + ":" + item.getValue()+","+item.getParameters());
                });

            }


        }


        System.out.println(controllerJavaClassList);
    }


}
