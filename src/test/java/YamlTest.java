import org.junit.Test;
import org.springrain.javadoc4openapi.JavaDoc4OpenAPI;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

public class YamlTest {

    @Test
    public void testYaml() throws FileNotFoundException {
        Yaml yaml = new Yaml();
        //Map<String, Object> ret = (Map<String, Object>) yaml.load(this.getClass().getClassLoader().getResourceAsStream("swagger2-config.yaml"));
        Map<String, Object> ret =  yaml.loadAs(new FileInputStream(new File("src/main/resources/swagger2-config.yaml")),Map.class);
        System.out.println(ret);
    }

    @Test
    public void testWrite() throws IOException {

        Map<String, Object> stringObjectMap = JavaDoc4OpenAPI.openAPIMap();
        JavaDoc4OpenAPI.makeYamlFileByOpenAPIMap(stringObjectMap,"D:/javadoc4openapi.yaml");
    }

}
